﻿// BattleForge Special Effect Editor
// Copyright(C) 2022 Skylords Reborn
// Project licensed under GNU General Public License v3.0. See LICENSE for more information.

using BattleForgeEffectEditor.Application.ViewModel.ElementSettings;
using BattleForgeEffectEditor.Models;
using BattleForgeEffectEditor.Models.Elements;

namespace BattleForgeEffectEditor.Application.ViewModel
{
    public class ElementEditorViewModel : ObservableObject
    {
        public StaticTrackListViewModel StaticTracksList { get; }

        public TrackListViewModel TracksList { get; }

        public NodeLinkViewModel NodeLink { get; }

        private DynamicTrackEditorViewModel dynamicTrackEditor;
        public DynamicTrackEditorViewModel DynamicTrackEditor
        {
            get => dynamicTrackEditor;
            set
            {
                dynamicTrackEditor = value;
                OnPropertyChanged();
            }
        }

        public bool ShowDynamicTrackEditor => TracksList.SelectedItem != null;

        private IElementSettings elementSettings;
        public IElementSettings ElementSettings
        {
            get => elementSettings;
            set
            {
                elementSettings = value;
                OnPropertyChanged();
            }
        }

        private IElement element;

        public ElementEditorViewModel()
        {
            StaticTracksList = new StaticTrackListViewModel();
            TracksList = new TrackListViewModel(this);
            NodeLink = new NodeLinkViewModel();
        }

        public void Update(ElementTreeItemViewModel treeElement)
        {
            element = treeElement.Element;

            StaticTracksList.Update(element);
            TracksList.Update(element);
            NodeLink.Update(element);

            ElementSettings = GetElementSettingsViewModel(treeElement);
        }

        public void UpdateDynamicTrackEditorVisiblity() => RaisePropertyChanged(() => ShowDynamicTrackEditor);

        private static IElementSettings GetElementSettingsViewModel(ElementTreeItemViewModel treeElement)
        {
            return treeElement.Element.GetType().Name switch
            {
                nameof(AnimatedMesh) => new AnimatedMeshSettingsViewModel(treeElement),
                nameof(Billboard) => new BillBoardSettingsViewModel(treeElement),
                nameof(Decal) => new DecalSettingsViewModel(treeElement),
                nameof(Effect) => new EffectSettingsViewModel(treeElement),
                nameof(Emitter) => new EmitterSettingsViewModel(treeElement),
                nameof(Light)  => new LightSettingsViewModel(treeElement),
                nameof(Mesh) => new MeshSettingsViewModel(treeElement),
                nameof(Physic) => new PhysicSettingsViewModel(treeElement),
                nameof(SfpSystem) => new SfpSystemSettingsViewModel(treeElement),
                nameof(Sound) => new SoundSettingsViewModel(treeElement),
                nameof(StaticDecal) => new StaticDecalSettingsViewModel(treeElement),
                nameof(Trail) => new TrailSettingsViewModel(treeElement),
                nameof(WaterDecal) => new WaterDecalSettingsViewModel(treeElement),
                nameof(SpecialEffect) => new SpecialEffectSettingsViewModel(treeElement),
                _ => new EmptySettingsViewModel(treeElement)
            };
        }
    }
}

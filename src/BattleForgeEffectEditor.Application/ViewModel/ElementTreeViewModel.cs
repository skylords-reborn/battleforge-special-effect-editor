﻿// BattleForge Special Effect Editor
// Copyright(C) 2022 Skylords Reborn
// Project licensed under GNU General Public License v3.0. See LICENSE for more information.

using BattleForgeEffectEditor.Application.Commands;
using BattleForgeEffectEditor.Application.Utility;
using BattleForgeEffectEditor.Models.Elements;
using GongSolutions.Wpf.DragDrop;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace BattleForgeEffectEditor.Application.ViewModel
{
    public class MenuItemViewModel : ObservableObject
    {
        public string Header { get; private set; }

        public ICommand Command { get; private set; }

        public MenuItemViewModel(string header, ICommand command)
        {
            Header = header;
            Command = command;
        }
    }

    public class ElementTreeViewModel : ObservableObject, IDropTarget
    {
        public ObservableCollection<ElementTreeItemViewModel> Elements { get; private set; }

        private ElementTreeItemViewModel selectedItem;
        public ElementTreeItemViewModel SelectedItem
        {
            get => selectedItem;
            set
            {
                if (selectedItem != null)
                    selectedItem.IsSelected = false;
                value.IsSelected = true;

                selectedItem = value;
                toggleAllPingPong = false;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<MenuItemViewModel> AddElementOptions { get; private set; }

        public bool IsNonRootNodeSelected => !SelectedItem?.IsRootNode ?? false;

        public bool IsANodeSelected => SelectedItem != null;

        public bool IsMoveDownEnabled => SelectedItem?.Parent != null && SelectedItem.Parent.Elements.IndexOf(SelectedItem) < SelectedItem.Parent.Elements.Count - 1;
        
        public bool IsMoveUpEnabled => SelectedItem?.Parent != null && SelectedItem.Parent.Elements.IndexOf(SelectedItem) > 0;

        public ICommand ExpandAllCommand => new RelayCommand((_) => ExpandAllChildren(), (_) => IsANodeSelected);

        public ICommand CollapseAllCommand => new RelayCommand((_) => CollapseAllChildren(), (_) => IsANodeSelected);

        public ICommand DeleteElementCommand => new RelayCommand((_) => DeleteSelectedElement(), (_) => IsNonRootNodeSelected);

        public ICommand CloneElementCommand => new RelayCommand((_) => CloneSelectedElement(), (_) => IsNonRootNodeSelected);

        public ICommand ToggleElementVisibilityCommand => new RelayCommand((_) => ToggleElementVisibility(), (_) => IsNonRootNodeSelected);

        public ICommand ToggleAllExceptSelectedElementCommand => new RelayCommand((_) => ToggleAllExceptSelectedElement(), (_) => IsNonRootNodeSelected);

        public ICommand MoveUpCommand => new RelayCommand((_) => MoveUp(), (_) => IsMoveUpEnabled);

        public ICommand MoveDownCommand => new RelayCommand((_) => MoveDown(), (_) => IsMoveDownEnabled);

        private ElementTreeItemViewModel rootElementNode;
        private bool toggleAllPingPong = false;

        private SpecialEffectEditorViewModel effectEditor;

        public ElementTreeViewModel(SpecialEffectEditorViewModel effectEditor)
        {
            this.effectEditor = effectEditor;

            rootElementNode = new ElementTreeItemViewModel(this, null, effectEditor, effectEditor.SpecialEffect);
            Elements = new ObservableCollection<ElementTreeItemViewModel>
            {
                rootElementNode
            };

            if (Elements.Count > 0)
                Elements[0].RecursiveExpand();

            InitializeElementContextMenuItems();
        }

        private void InitializeElementContextMenuItems()
        {
            AddElementOptions = new ObservableCollection<MenuItemViewModel>();

            foreach (Type elementType in AppDomain.CurrentDomain.GetAssemblies()
                       .SelectMany(assembly => assembly.GetTypes())
                       .Where(type => type.IsSubclassOf(typeof(Element))))
            {
                ICommand relayCommand = new RelayCommand((_) =>
                {
                    AddElement(elementType);
                });
                AddElementOptions.Add(new MenuItemViewModel(elementType.Name, relayCommand));
            }
        }

        public void CloneSelectedElement()
        {
            IElement clonedElement = SelectedItem.Element.Copy();
            clonedElement.Name += "_New";

            ElementTreeItemViewModel treeItem = new ElementTreeItemViewModel(
                this, SelectedItem.Parent, effectEditor, clonedElement);
            SelectedItem.Parent.Element.Children.Add(clonedElement);
            SelectedItem.Parent.Elements.Add(treeItem);

            SelectedItem = treeItem;
        }

        private void ToggleElementVisibility()
        {
            SelectedItem.RecursiveToggleElementEnable();
        }

        private void ToggleAllExceptSelectedElement()
        {
            foreach (ElementTreeItemViewModel child in rootElementNode.Elements)
                child.RecursiveSetElementEnable(toggleAllPingPong);

            toggleAllPingPong = !toggleAllPingPong;
            SelectedItem.RecursiveSetElementEnable(true);
        }

        private void ExpandAllChildren()
        {
            SelectedItem.RecursiveExpand();
        }

        private void CollapseAllChildren()
        {
            SelectedItem.RecursiveCollapse();
        }

        public void DeleteSelectedElement()
        {
            ElementTreeItemViewModel parent = SelectedItem.Parent;

            parent.Element.Children.Remove(SelectedItem.Element);
            parent.Elements.Remove(SelectedItem);

            ElementTreeItemViewModel newSelection = parent.Elements.LastOrDefault();
            if (newSelection != null)
                SelectedItem = newSelection;
        }

        public void AddElement(Type elementType)
        {
            if (!IsANodeSelected || !elementType.IsSubclassOf(typeof(Element)))
                return;

            //TODO Indicate this better in the UI.
            //if ((elementType == typeof(SfpForceField) || elementType == typeof(SfpEmitter))
            //    && SelectedItem.Element.GetType() != typeof(SfpSystem))
            //    return;

            Element element = (Element)Activator.CreateInstance(elementType);
            element.Parent = SelectedItem.Element;
            SelectedItem.Element.Children.Add(element);

            SelectedItem.Elements.Add(new ElementTreeItemViewModel(this, SelectedItem, effectEditor, element));
            SelectedItem.IsExpanded = true;
        }

        private void SwapItemsInList<T>(IList<T> list, int firstIndex, int secondIndex)
        {
            var firstItem = list[firstIndex];
            list[firstIndex] = list[secondIndex];
            list[secondIndex] = firstItem;
        }

        public void MoveUp() 
        {
            var oldIndex = selectedItem.Parent.Elements.IndexOf(selectedItem);
            var newIndex = oldIndex - 1;
            SwapItemsInList(selectedItem.Parent.Elements, oldIndex, newIndex);
            SwapItemsInList(selectedItem.Parent.Element.Children, oldIndex, newIndex);
        }

        public void MoveDown()
        {
            var oldIndex = selectedItem.Parent.Elements.IndexOf(selectedItem);
            var newIndex = oldIndex + 1;
            SwapItemsInList(selectedItem.Parent.Elements, oldIndex, newIndex);
            SwapItemsInList(selectedItem.Parent.Element.Children, oldIndex, newIndex);
        }

        public void DragOver(IDropInfo dropInfo)
        {
            var sourceItem = dropInfo.Data as ElementTreeItemViewModel;
            var targetItem = dropInfo.TargetItem as ElementTreeItemViewModel;
            var dropAsChild = dropInfo.InsertPosition.HasFlag(RelativeInsertPosition.TargetItemCenter);

            dropInfo.DropTargetAdorner = dropAsChild ? DropTargetAdorners.Highlight : DropTargetAdorners.Insert;

            if (!IsDropValid(sourceItem, targetItem, dropAsChild))
                dropInfo.Effects = DragDropEffects.None;
            else
                dropInfo.Effects = DragDropEffects.Move;
        }

        private bool IsDropValid(ElementTreeItemViewModel sourceItem, ElementTreeItemViewModel targetItem, bool dropAsChild)
        {
            if (!dropAsChild && targetItem?.Parent == null)
            {
                return false;
            }

            return !(sourceItem == null ||
                    targetItem == null ||
                    sourceItem == targetItem ||
                    sourceItem.HasTreeItemAsChild(targetItem) ||
                    sourceItem.IsRootNode);
        }

        public void Drop(IDropInfo dropInfo)
        {
            var sourceItem = dropInfo.Data as ElementTreeItemViewModel;
            var targetItem = dropInfo.TargetItem as ElementTreeItemViewModel;
            var dropAsChild = dropInfo.InsertPosition.HasFlag(RelativeInsertPosition.TargetItemCenter);

            if (!IsDropValid(sourceItem, targetItem, dropAsChild))
                return;

            if (dropInfo.InsertPosition.HasFlag(RelativeInsertPosition.TargetItemCenter))
            {
                InsertAsChild(sourceItem, targetItem);
            }
            else
            {
                InsertAsSibling(sourceItem, targetItem, dropInfo.InsertPosition);
            }

            if (sourceItem.EffectEditor != targetItem.EffectEditor)
                targetItem.EffectEditor.Reparent(sourceItem);

            sourceItem.TreeView.SelectedItem = sourceItem;
        }

        private void InsertAsChild(ElementTreeItemViewModel sourceItem, ElementTreeItemViewModel targetItem)
        {
            sourceItem.Parent.Elements.Remove(sourceItem);
            sourceItem.Parent.Element.Children.Remove(sourceItem.Element);

            sourceItem.Parent = targetItem;
            sourceItem.Element.Parent = targetItem.Element;

            targetItem.Elements.Add(sourceItem);
            targetItem.Element.Children.Add(sourceItem.Element);
        }

        private void InsertAsSibling(ElementTreeItemViewModel sourceItem, ElementTreeItemViewModel targetItem, RelativeInsertPosition insertPosition)
        {
            sourceItem.Parent.Elements.Remove(sourceItem);
            sourceItem.Parent.Element.Children.Remove(sourceItem.Element);

            sourceItem.Parent = targetItem.Parent;
            sourceItem.Element.Parent = targetItem.Parent.Element;

            var targetItemIndex = targetItem.Parent.Elements.IndexOf(targetItem);
            var targetItemElementIndex = targetItem.Parent.Element.Children.IndexOf(targetItem.Element);

            if (insertPosition == RelativeInsertPosition.AfterTargetItem)
            {
                targetItemIndex++;
                targetItemElementIndex++;
            }

            targetItem.Parent.Elements.Insert(targetItemIndex ,sourceItem);
            targetItem.Parent.Element.Children.Insert(targetItemElementIndex, sourceItem.Element);
        }

        public void HandleKeyshortcut(Key pressedKey)
        {
            if (!IsANodeSelected)
                return;

            switch (pressedKey)
            {
                case Key.V:
                    SelectedItem.RecursiveToggleElementEnable();
                    break;
                case Key.C:
                    CloneSelectedElement();
                    break;
                case Key.D:
                    DeleteSelectedElement();
                    break;
                default: break;
            }
        }

        public void Update(ElementTreeItemViewModel selectedItem)
        {
            SelectedItem = selectedItem;
            RaisePropertyChanged(() => IsANodeSelected);
        }
    }
}

﻿// BattleForge Special Effect Editor
// Copyright(C) 2023 Skylords Reborn
// Project licensed under GNU General Public License v3.0. See LICENSE for more information.

using Microsoft.Xaml.Behaviors;
using System.Windows.Controls;

namespace BattleForgeEffectEditor.Application.Behaviors
{
    public class ScrollIntoViewBehavior : Behavior<ListView>
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.SelectionChanged += ScrollIntoView;
        }

        protected override void OnDetaching()
        {
            AssociatedObject.SelectionChanged -= ScrollIntoView;
            base.OnDetaching();
        }

        private void ScrollIntoView(object o, SelectionChangedEventArgs e)
        {
            ListView targetListView = (ListView)o;
            if (targetListView == null)
                return;

            if (targetListView.SelectedItem == null)
                return;

            var item = targetListView.Items[targetListView.SelectedIndex];
            if (item != null)
            {
                targetListView.ScrollIntoView(item);
            }
        }
    }
}

﻿using System;
using System.IO;
using System.Text;

namespace BattleForgeEffectEditor.Models.Utility
{
    public static class BinaryReaderExtension
    {
        public static string ReadBfString(this BinaryReader reader)
        {
            int count = reader.ReadInt32();
            if (count == 0)
                return string.Empty;
            return Encoding.ASCII.GetString(reader.ReadBytes(count));
        }

        public static Vector3 ReadVector3(this BinaryReader reader)
        {
            return new Vector3
            {
                X = reader.ReadSingle(),
                Y = reader.ReadSingle(),
                Z = reader.ReadSingle(),
            };
        }

        public static T ReadEnumValue<T>(this BinaryReader reader)
        {
            Type type = typeof(T);
            return (T)Enum.ToObject(type, reader.ReadStructureValue(type.GetEnumUnderlyingType()));
        }

        public static object ReadStructureValue(this BinaryReader reader, Type type)
        {
            if (type == typeof(string))
                return reader.ReadBfString();
            if (type == typeof(sbyte))
                return reader.ReadSByte();
            if (type == typeof(byte))
                return reader.ReadByte();
            if (type == typeof(bool))
                return reader.ReadBoolean();
            if (type == typeof(ushort))
                return reader.ReadUInt16();
            if (type == typeof(short))
                return reader.ReadInt16();
            if (type == typeof(uint))
                return reader.ReadUInt32();
            if (type == typeof(int))
                return reader.ReadInt32();
            if (type == typeof(long))
                return reader.ReadInt64();
            if (type == typeof(ulong))
                return reader.ReadUInt64();
            if (type == typeof(float))
                return reader.ReadSingle();
            throw new ArgumentException(type.FullName);
        }

        public static T Peek<T>(this BinaryReader reader) where T : struct
        {
            if (typeof(T).IsCustomValueType())
                throw new ArgumentException(typeof(T) + " not supported for Peek");
            long position = reader.BaseStream.Position;
            T value = (T)reader.ReadStructureValue(typeof(T));
            reader.BaseStream.Seek(position, SeekOrigin.Begin);
            return value;
        }
    }
}
